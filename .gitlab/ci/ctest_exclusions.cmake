set(test_exclusions)

list(APPEND test_exclusions
  # WIN32 Needs looked at.
  # Maybe https://gitlab.kitware.com/paraview/paraview/merge_requests/3728 fixes it?
  # All platforms: turning off ObjectPickingBehavior, https://gitlab.kitware.com/cmb/cmb/-/merge_requests/834
  # breaks the selection/deselection recorded in the test. Needs re-recording.
  "ColorKnee")

if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "paraview59")
  list(APPEND test_exclusions "OscillatorSingle;ModelSelection")
endif ()

string(REPLACE ";" "|" test_exclusions "${test_exclusions}")
if (test_exclusions)
  set(test_exclusions "(${test_exclusions})")
endif ()
